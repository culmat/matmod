# matmod

a mod allowing to import 3d models to minetest

# usage

*Models* are loaded from <worldpath>/schems/<name>.txt or <modpath>/schems/<name>.txt
A model file contains one pixel per line in x,y,z coordinates and optionally r,g,b values, see  [azer.txt](https://codeberg.org/culmat/matmod/src/branch/main/schems/azer.txt) and [azer_rgb.txt](https://codeberg.org/culmat/matmod/src/branch/main/schems/azer_rgb.txt)

Material is either a mcl  material name ( without mcl prefix) like you can find in [nodes_base.lua](https://git.minetest.land/MineClone2/MineClone2/src/branch/master/mods/ITEMS/mcl_core/nodes_base.lua) or a [palette](https://codeberg.org/culmat/matmod/src/branch/main/palettes.lua) with *pal:* prefix. The default material is *mcl_core:stone*.

`/place <name> <material>` - loads model file for placement on next punch
ie `/place azer` or `/place azer goldblock` or for rgb models only `/place azer_rgb pal:concrete`


now punch the node, where you want to place the model. Look around, sometimes the models are bhind you and surprisingly big.

`/place scale <percentage>` - sclaes the last placed model in place, <percentage> is either 0..1 or 1..100

`/place undo` - removes the last placed model. You may need to fly into 

`/place shift <x>,<y>,<z>` - moves the last placed model around ie /place shift <x>,<y>,<z>`

`/place mult <x>,<y>,<z>` - multiplies the coordinate matrix , ie `/place mult 1,-1,1` turns the model upside down (you might want to `/place shift 0,50,0` or similar to get it out of the ground)

`/place flip` - rotates the model +/- 90° in the x/z plane while preserving y (the height)

`/place clear` - clear the undo cache 


