local modPath = minetest.get_modpath(minetest.get_current_modname())
local worldPath = minetest.get_worldpath()
local palettes = dofile(modPath .. "/palettes.lua")
local undoCache
local nodes
local origin
-- -----------------------------------------------------------------------------------------------------

local function distance(c1, c2)
  return math.sqrt(math.pow((c2.r - c1.r), 2) + math.pow((c2.g - c1.g), 2) + math.pow((c2.b - c1.b), 2))
end

local function minPos(pos1, pos2)
  return { 
          x = pos1.x and math.min(pos1.x,pos2.x) or pos2.x,
          y = pos1.y and math.min(pos1.y,pos2.y) or pos2.y,
          z = pos1.z and math.min(pos1.z,pos2.z) or pos2.z
          }
end

local function maxPos(pos1, pos2)
  return { 
          x = pos1.x and math.max(pos1.x,pos2.x) or pos2.x,
          y = pos1.y and math.max(pos1.y,pos2.y) or pos2.y,
          z = pos1.z and math.max(pos1.z,pos2.z) or pos2.z
          }
end


local closestCache = {}
local function closest(color, palette)
  local hashKey = color.r * 1000000 + color.b * 1000 + color.g
  closestCache[palette] = closestCache[palette] or {}
  if(closestCache[palette][hashKey]) then
    return closestCache[palette][hashKey]
  end
  local min = 999
  local closest
  for m, c in pairs(palettes[palette]) do
    local distance = distance(color,c)
    min = math.min(min,distance)
    if(min == distance) then
      closest = m
    end
  end
  closestCache[palette][hashKey] = closest
  return closest
end

local function getMaterial(r,g,b, desc)
  local fallback = "mcl_core:stone"
  if not desc or desc == "" then return fallback end
  local pal = string.match(desc,"pal:(.+)")
  if pal then
    return (r and r~="") and closest({ r=r, b=b,g=g }, pal) or fallback
  elseif not string.find(desc, ':') then
    return "mcl_core:"..desc
  else
    return desc
  end
end

function file_open(name)
   return io.open(worldPath .. name,"r") or io.open(modPath .. name,"r")
end


function file_exists(name)
 local f=file_open(name)
 if f then 
    io.close(f) 
    return true 
  else 
    return false 
  end
end


function createCache() 
  local ret = {cache = {}, bbox = {min={}, max={} } }
  ret.set_node = function (pos, node, existingNode)
    ret.cache[pos] = existingNode or minetest.get_node(pos)
    ret.bbox.min = minPos(ret.bbox.min,pos)
    ret.bbox.max = maxPos(ret.bbox.max,pos)
    minetest.set_node(pos,node)
  end
  ret.bulk_set_node = function (positions, node)
    for _, pos in ipairs(positions) do
      ret.cache[pos] = minetest.get_node(pos)
      ret.bbox.min = minPos(ret.bbox.min,pos)
      ret.bbox.max = maxPos(ret.bbox.max,pos)
    end
    minetest.bulk_set_node(positions,node)
  end
  ret.undo = function ()
    for pos, node in pairs(ret.cache) do
      if node.name == "ignore" then
        minetest.remove_node(pos)
      else
        minetest.set_node(pos,node)
      end
    end
    ret.fix_light()
  end
  ret.fix_light = function ()
    minetest.fix_light(vector.subtract(ret.bbox.min,15), vector.add(ret.bbox.max,15))
  end
  return ret
end

function fundament(basePos,mat) 
  local pos
  for i=1,11 do 
    pos = {x= basePos.x, y= basePos.y-i, z=basePos.z}
    local existingNode = minetest.get_node(pos)
    if existingNode.name == "air" then
      table.insert(nodes[mat], pos) 
    else
      return  
    end  
  end
end

function shiftOrigin(x,y,z)
  origin = vector.add(origin, vector.new(x,y,z))
  shift(x, y, z)
end 

local function unique(positions)
  local hash = {}
  local res = {}
  for _,pos in ipairs(positions) do
     local h = minetest.pos_to_string(pos)
     if (not hash[h]) then
         table.insert(res,pos)
         hash[h] = true
     end
  end
  return res
end

function shift(x,y,z) 
  for material, positions in pairs(nodes) do
      for _, pos in ipairs(positions) do
        pos.x = pos.x + x
        pos.y = pos.y + y
        pos.z = pos.z + z
      end
  end
end

function multiply(x,y,z)
  for material, positions in pairs(nodes) do
      for _, pos in ipairs(positions) do
        pos.x = math.floor((pos.x - origin.x) * x) + origin.x
        pos.y = math.floor((pos.y - origin.y) * y) + origin.y
        pos.z = math.floor((pos.z - origin.z) * z) + origin.z
      end
      nodes[material] = unique(nodes[material])
  end
end

function flip()
  for material, positions in pairs(nodes) do
      for _, pos in ipairs(positions) do
        pos.z, pos.x   = pos.x - origin.x + origin.z, pos.z - origin.z + origin.x
      end
  end
end

function scale(s)
  s = 1 * s
  s = s > 1 and s / 100 or s 
  multiply(s,s,s)
end

function place()
  minetest.debug("placing", filename, material)
  undoCache = createCache()
  local start = minetest.get_us_time()
  for material, positions in pairs(nodes) do
      undoCache.bulk_set_node(positions, {name = material})
  end
  undoCache.fix_light()
  minetest.debug("placing duration", minetest.get_us_time()-start)
end

function parseFile(filename, material)
  local f = file_open(filename)
  if not f then return nil end
  local content = f:read("*all")
  f:close()
  minetest.debug("parsing", filename, material)
  nodes  = {}
  local start = minetest.get_us_time()
  local mat, tpos
  for line in content:gmatch('[^\r\n]+') do
    for x,y,z,r,g,b in line:gmatch('(%d+)[ ,]+(%d+)[ ,]+(%d+)[ ,]*(%d*)[ ,]*(%d*)[ ,]*(%d*)') do
      mat = getMaterial(r,g,b, material)
      tpos = {x = x, y = y, z = z}
      nodes[mat] = nodes[mat] or {}
      table.insert(nodes[mat], tpos) 
      if y=="0" then
        fundament(tpos,mat)
      end
    end
  end
  minetest.debug("parsing duration", minetest.get_us_time()-start)
  return true
end

-- -----------------------------------------------------------------------------------------------------

local onPunch = {}
minetest.register_chatcommand('place', {
  params = "<file> <material>",

  description = "loads file for placement on next punch",

  privs = {},

  func = function(playerName, param)
    minetest.chat_send_player(playerName, param)
    if param == "undo" then
      if undoCache then 
        undoCache.undo()
        clear()
      else
        minetest.chat_send_player(playerName, "nothing to undo")      
      end
    elseif param == "clear" then
      clear()
    else
      local p1, p2 =  param:match("(%S+)%s*(%S*)")
      if(p1 == "shift") then
        undoCache.undo()
        shiftOrigin(p2:match("([^,]+),([^,]+),([^,]+)"))
        place()
      elseif (p1 == "mult") then
        undoCache.undo()
        multiply(p2:match("([^,]+),([^,]+),([^,]+)"))
        place()
      elseif (p1 == "scale") then
        undoCache.undo()
        scale(p2)
        place()
      elseif (p1 == "flip") then
        undoCache.undo()
        flip()
        place()
      else
        onPunch[playerName] = parseFile('/schems/' ..p1 .. ".txt", p2)
        if not onPunch[playerName] then
          minetest.chat_send_player(playerName, "file not found: "..p1)
        end
      end
    end
  end

})

function clear() undoCache = nil; nodes=nil end

minetest.register_on_punchnode(function(pos, node, puncher, pointed_thing)
  local playerName = puncher:get_player_name()
  if(not onPunch[playerName]) then return end
  local params = onPunch[playerName]
  onPunch[playerName] = nil
  minetest.after(1, function()
    minetest.set_node(pos,node)
  end)
  origin = pos
  shift(origin.x, origin.y, origin.z)
  place()
end)

    